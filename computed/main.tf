locals {
    env = var.environment
    first_name = var.first_name
    last_name = var.last_name
    user_id = var.user_id
}