output "name" {
  value = "${local.first_name}-${local.last_name}"
}

output "name_with_user_id_with_dashes" {
  value = "${local.first_name}-${local.user_id}"
}

output "name_with_user_id" {
  value = "${local.first_name}${local.user_id}"
}

output "user_info_with_env_with_dashes" {
  value = "${local.first_name}-${local.user_id}-${local.env}"
}

output "user_info_with_env" {
  value = "${local.first_name}${local.user_id}${local.env}"
}