resource "azurerm_storage_account" "source_sa" {
  name                      = var.sa_name
  resource_group_name       = var.resource_group.name
  location                  = var.resource_group.location
  account_tier              = "Standard"
  account_replication_type  = "GRS"

  tags = var.common_tags
}
